# README #

Mule Project with the Verification API components 

### What is this repository for? ###

* Quick demo of how the Verification API will look
* How the Cloud build API in run in MuleStudio

### How do I get set up? ###

* Run Anypoint Studio
* Import project

### Contribution guidelines ###

* Build the API in Anypoint Cloud [https://anypoint.mulesoft.com/apiplatform/sixtree-api/admin] 
* Download API files (api.raml, schemas/, examples/)
* Create new Mule Project

### Who do I talk to? ###

* Repo Alok Mishra